package com.khmelenkoschlatter.hackzurich2015.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.adapter.OnItemListListener;
import com.khmelenkoschlatter.hackzurich2015.adapter.SensorsListAdapter;
import com.khmelenkoschlatter.hackzurich2015.engine.RegistrationIntentService;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.SensorsSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.SensorResponse;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * @author Dmytro Khmelenko
 */
public class SensorsActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Bind(R.id.sensors_sensors_list)
    RecyclerView mSensorsList;

    @Bind(R.id.sensors_add_sensor_btn)
    FloatingActionButton mAddSensorBtn;

    private SensorsListAdapter mSensorsListAdapter;
    private List<SensorResponse> mSensors = new ArrayList<>();

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);
        ButterKnife.bind(this);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(RegistrationIntentService.SENT_TOKEN_TO_SERVER, false);
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        mSensorsList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mSensorsList.setLayoutManager(layoutManager);


        mSensorsListAdapter = new SensorsListAdapter(this, mSensors, new OnItemListListener() {
            @Override
            public void onItemSelected(int position) {
                SensorResponse selected = mSensors.get(position);

                Intent intent = new Intent(SensorsActivity.this, HistoryActivity.class);
                intent.putExtra(HistoryActivity.SENSOR_NAME_KEY, selected.getClientId());
                startActivity(intent);
            }
        });
        mSensorsList.setAdapter(mSensorsListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));

        TaskLauncher.getSensors();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @OnClick(R.id.sensors_add_sensor_btn)
    public void doAddSensor() {
        Intent intent = new Intent(this, AddSensorActivity.class);
        startActivity(intent);
    }

    public void onEvent(SensorsSuccessEvent event) {

        mSensors.clear();
        mSensors.addAll(event.getSensors());
        mSensorsListAdapter.notifyDataSetChanged();
    }

    public void onEvent(LoadingFailedEvent event) {
        Toast.makeText(this, "Getting sensors failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("hack", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
