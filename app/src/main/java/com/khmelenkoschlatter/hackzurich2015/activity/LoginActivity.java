package com.khmelenkoschlatter.hackzurich2015.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.khmelenkoschlatter.hackzurich2015.AppPreferences;
import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoginSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AccountResponse;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.txt_email)
    EditText mTxtEmail;

    @Bind(R.id.txt_password)
    EditText mTxtPassword;

    @Bind(R.id.btn_login)
    Button mBtnLogin;

    @Bind(R.id.link_signup)
    TextView mLinkSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        if (AppPreferences.getAuthorizationHeader() != null) {
            Intent intent = new Intent(this, SensorsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        if (isDataValid()) {
            mBtnLogin.setEnabled(false);
            TaskLauncher.login(mTxtEmail.getText().toString(), mTxtPassword.getText().toString());
        }
    }

    @OnClick(R.id.link_signup)
    public void goToRegister() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean isDataValid() {
        boolean dataValid = true;
        if (TextUtils.isEmpty(mTxtEmail.getText())) {
            mTxtEmail.setError("Email cannot be empty");
            dataValid = false;
        }

        if (TextUtils.isEmpty(mTxtPassword.getText())) {
            mTxtPassword.setError("Password cannot be empty");
            dataValid = false;
        }

        return dataValid;
    }

    public void onEvent(LoginSuccessEvent event) {
        Log.d("hack", "Login success");

        // create Base64 encoded string
        AccountResponse accountResponse = event.getLoginResponse().getAccountResponse();
        String credentials = accountResponse.getEmail() + ":" + accountResponse.getPassword();
        String authHeader = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        AppPreferences.saveAuthorizationHeader(authHeader);

        mBtnLogin.setEnabled(true);
        Intent intent = new Intent(this, SensorsActivity.class);
        startActivity(intent);
    }

    public void onEvent(LoadingFailedEvent event) {
        mBtnLogin.setEnabled(true);
        Toast.makeText(this, "Login failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
