package com.khmelenkoschlatter.hackzurich2015.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.SensorsSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * @author Dmytro Khmelenko
 */
public class AddSensorActivity extends AppCompatActivity {

    @Bind(R.id.add_sensor_sensor_name)
    EditText mSensorName;

    @Bind(R.id.add_sensor_add_btn)
    Button mAddBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sensor);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.add_sensor_add_btn)
    public void addSensor() {
        if(isDataValid()) {
            mAddBtn.setEnabled(false);
            TaskLauncher.addSensor(mSensorName.getText().toString());
        }
    }

    private boolean isDataValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(mSensorName.getText())) {
            mSensorName.setError("Sensor name cannot be empty");
            valid = false;
        }
        return valid;
    }

    public void onEvent(SensorsSuccessEvent event) {
        // just go back
        mAddBtn.setEnabled(true);
        finish();
    }

    public void onEvent(LoadingFailedEvent event) {
        mAddBtn.setEnabled(true);
        Toast.makeText(this, "Adding sensor failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
