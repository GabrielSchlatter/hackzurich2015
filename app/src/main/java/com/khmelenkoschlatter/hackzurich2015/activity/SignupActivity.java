package com.khmelenkoschlatter.hackzurich2015.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.khmelenkoschlatter.hackzurich2015.AppPreferences;
import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.RegistrationSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AccountResponse;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by Donga on 03.10.2015.
 */
public class SignupActivity extends AppCompatActivity {

    @Bind(R.id.txt_email)
    EditText mTxtEmail;

    @Bind(R.id.txt_password)
    EditText mTxtPassword;

    @Bind(R.id.btn_signup)
    Button mBtnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_signup)
    public void doSignUp() {
        if (isDataValid()) {
            mBtnSignup.setEnabled(false);
            TaskLauncher.register(mTxtEmail.getText().toString(), mTxtPassword.getText().toString());
        }
    }

    private boolean isDataValid() {
        boolean dataValid = true;
        if (TextUtils.isEmpty(mTxtEmail.getText())) {
            mTxtEmail.setError("Email cannot be empty");
            dataValid = false;
        }

        if (TextUtils.isEmpty(mTxtPassword.getText())) {
            mTxtPassword.setError("Password cannot be empty");
            dataValid = false;
        }

        return dataValid;
    }

    public void onEvent(RegistrationSuccessEvent event) {
        Log.d("hack", "Registration success");

        // create Base64 encoded string
        AccountResponse accountResponse = event.getResponse().getAccountResponse();
        String credentials = accountResponse.getEmail() + ":" + accountResponse.getPassword();
        String authHeader = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        AppPreferences.saveAuthorizationHeader(authHeader);

        mBtnSignup.setEnabled(true);
        Intent intent = new Intent(this, SensorsActivity.class);
        startActivity(intent);
    }

    public void onEvent(LoadingFailedEvent event) {
        mBtnSignup.setEnabled(true);
        Toast.makeText(this, "Registration failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
