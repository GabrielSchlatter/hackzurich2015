package com.khmelenkoschlatter.hackzurich2015.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.adapter.HistoryListAdapter;
import com.khmelenkoschlatter.hackzurich2015.event.HistorySuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.WeatherDataResponse;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * @author Dmytro Khmelenko
 */
public class HistoryActivity extends AppCompatActivity {

    public static final String SENSOR_NAME_KEY = "SensorNameKey";

    @Bind(R.id.history_history_list)
    RecyclerView mHistoryList;

    private String mSensorName;

    private HistoryListAdapter mHistoryListAdapter;
    private List<WeatherDataResponse> mHistoryItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        mSensorName = getIntent().getStringExtra(SENSOR_NAME_KEY);

        mHistoryList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mHistoryList.setLayoutManager(layoutManager);

        mHistoryListAdapter = new HistoryListAdapter(this, mHistoryItems);
        mHistoryList.setAdapter(mHistoryListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        TaskLauncher.getHistory(mSensorName);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.history_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history_item_settings:
                Intent intent = new Intent(this, SensorSettingsActivity.class);
                intent.putExtra(SensorSettingsActivity.SENSOR_NAME_KEY, mSensorName);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onEvent(HistorySuccessEvent event) {

        mHistoryItems.clear();
        mHistoryItems.addAll(event.getHistoryData());
        mHistoryListAdapter.notifyDataSetChanged();
    }

    public void onEvent(LoadingFailedEvent event) {
        Toast.makeText(this, "Getting sensors failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
