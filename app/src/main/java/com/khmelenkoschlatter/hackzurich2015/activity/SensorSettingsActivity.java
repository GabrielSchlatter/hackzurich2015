package com.khmelenkoschlatter.hackzurich2015.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.event.FetchAleftConfigSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.UpdateAlertConfigSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.request.UpdateAlertConfigRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.hackzurich2015.task.TaskLauncher;
import com.khmelenkoschlatter.hackzurich2015.view.RangeSeekBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * @author Dmytro Khmelenko
 */
public class SensorSettingsActivity extends AppCompatActivity {

    public static final String SENSOR_NAME_KEY = "SensorName";

    @Bind(R.id.sensor_settings_enable_temperature_alarm)
    CheckBox mEnableTemperature;

    @Bind(R.id.sensor_settings_temperature_range)
    RangeSeekBar<Integer> mTemperatureRange;

    @Bind(R.id.sensor_settings_enable_humidity_alarm)
    CheckBox mEnableHumidity;

    @Bind(R.id.sensor_settings_humidity_range)
    RangeSeekBar<Integer> mHumidityRange;

    @Bind(R.id.sensor_settings_done_btn)
    Button mDoneBtn;

    private String mSensorName;

    private AlertConfigResponse mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_settings);
        ButterKnife.bind(this);

        mSensorName = getIntent().getStringExtra(SENSOR_NAME_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        TaskLauncher.fetchAlertConfig(mSensorName);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.sensor_settings_done_btn)
    public void onDone() {
        mDoneBtn.setEnabled(false);

        AlertConfigResponse newData = new AlertConfigResponse();
        newData.setIsTemperatureAlertEnabled(mEnableTemperature.isChecked());
        newData.setMaxTemperature(mTemperatureRange.getSelectedMaxValue());
        newData.setMinTemperature(mTemperatureRange.getSelectedMinValue());

        newData.setIsHumidityAlertEnabled(mEnableHumidity.isChecked());
        newData.setMaxHumidity(mHumidityRange.getSelectedMaxValue());
        newData.setMinHumidity(mHumidityRange.getSelectedMinValue());

        TaskLauncher.updateAlertConfig(mSensorName, newData);
    }

    public void onEvent(FetchAleftConfigSuccessEvent event) {
        Log.d("hack", "Sensor data fetched");

        mConfig = event.getAlertConfig();

        mEnableTemperature.setChecked(mConfig.isTemperatureAlertEnabled());
        mTemperatureRange.setSelectedMaxValue((int) mConfig.getMaxTemperature());
        mTemperatureRange.setSelectedMinValue((int) mConfig.getMinTemperature());

        mEnableHumidity.setChecked(mConfig.isHumidityAlertEnabled());
        mHumidityRange.setSelectedMaxValue((int) mConfig.getMaxHumidity());
        mHumidityRange.setSelectedMinValue((int)mConfig.getMinHumidity());
    }

    public void onEvent(UpdateAlertConfigSuccessEvent event) {
        Log.d("hack", "Sensor data updated");
        mDoneBtn.setEnabled(true);
        finish();
    }

    public void onEvent(LoadingFailedEvent event) {
        mDoneBtn.setEnabled(true);
        Toast.makeText(this, "Getting sensor data failed: " + event.getTaskError().getMessage(), Toast.LENGTH_SHORT).show();
    }
}
