package com.khmelenkoschlatter.hackzurich2015.error;

/**
 * Define the error which occurred during task execution
 */
public final class TaskError {
    private final int mCode;
    private final String mMessage;

    public TaskError(int code, String message) {
        mCode = code;
        mMessage = message;
    }

    public int getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }
}