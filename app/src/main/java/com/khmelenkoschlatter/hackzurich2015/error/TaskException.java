package com.khmelenkoschlatter.hackzurich2015.error;

/**
 * Defines an exception which occurred during task execution
 */
public class TaskException extends RuntimeException {

    private final int mCode;
    private final String mMessage;

    public TaskException(int code, String message) {
        mCode = code;
        mMessage = message;
    }

    public int getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }
}