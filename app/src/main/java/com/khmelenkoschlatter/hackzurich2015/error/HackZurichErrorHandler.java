package com.khmelenkoschlatter.hackzurich2015.error;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Donga on 03.10.2015.
 */
public class HackZurichErrorHandler implements ErrorHandler {

    @Override
    public Throwable handleError(RetrofitError cause) {
        int status = 0;
        String message = null;
        if (cause != null) {
            message = cause.getMessage();
            Response response = cause.getResponse();
            if (response != null) {
                status = response.getStatus();
            }
        }
        return new TaskException(status, message);
    }
}
