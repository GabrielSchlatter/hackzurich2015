package com.khmelenkoschlatter.hackzurich2015.network;

import android.util.Base64;

import com.khmelenkoschlatter.hackzurich2015.BuildConfig;
import com.khmelenkoschlatter.hackzurich2015.HackZurichApp;
import com.khmelenkoschlatter.hackzurich2015.error.HackZurichErrorHandler;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Donga on 03.10.2015.
 */
public class BackendApi {

    private static final String ENDPOINT = "http://192.168.43.219:8080";
    private static final long CACHE_SIZE = 100 * 1024 * 1024;

    public static BackendApi instance;
    private HackZurichService mService;

    private static String sAuthHeader;

    public static BackendApi getInstance(String authorizationHeader) {
        if (instance == null || sAuthHeader == null || !sAuthHeader.equals(authorizationHeader)) {
            instance = new BackendApi(authorizationHeader);
        }
        return instance;
    }

    public static BackendApi getInstance() {
        if (instance == null || sAuthHeader != null) {
            instance = new BackendApi(null);
        }
        return instance;
    }

    private BackendApi(String authorizationHeader) {
        sAuthHeader = authorizationHeader;

        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(ENDPOINT);
        builder.setErrorHandler(new HackZurichErrorHandler());
        builder.setClient(getOkClient());

        if (authorizationHeader != null) {

            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestInterceptor.RequestFacade request) {
                    request.addHeader("Authorization", sAuthHeader);
                    request.addHeader("Accept", "application/json");
                }
            });
        }

        if (BuildConfig.DEBUG) {
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
        }
        mService = builder.build().create(HackZurichService.class);
    }

    public HackZurichService getService() {
        return mService;
    }

    private OkClient getOkClient() {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);

        try {
            File okHttpCache = new File(HackZurichApp.getAppContext().getCacheDir(), "okhttp");

            Cache cache = new Cache(okHttpCache, CACHE_SIZE);

            okHttpClient.setCache(cache);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new OkClient(okHttpClient);
    }
}
