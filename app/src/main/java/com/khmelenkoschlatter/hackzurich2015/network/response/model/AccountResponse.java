package com.khmelenkoschlatter.hackzurich2015.network.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class AccountResponse {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public AccountResponse() {

    }

    public AccountResponse(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
