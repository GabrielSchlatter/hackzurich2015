package com.khmelenkoschlatter.hackzurich2015.network.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class SensorResponse {

    @SerializedName("clientId")
    private String clientId;

    public SensorResponse() {

    }

    public SensorResponse(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }
}
