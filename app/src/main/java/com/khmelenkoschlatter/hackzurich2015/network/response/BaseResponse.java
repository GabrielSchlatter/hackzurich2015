package com.khmelenkoschlatter.hackzurich2015.network.response;

/**
 * Created by Donga on 03.10.2015.
 */
public class BaseResponse {

    public static final int SUCCESS = 111;
    public static final int FAILURE_RESSOURCE_EXISTS = 222;
    public static final int FAILURE_INVALID_REQUEST = 333;
    public static final int FAILURE_RESSOURCE_NOT_EXISTS = 444;
    public static final int FAILURE_AUTHENTICATION = 555;

    protected int result;

    public BaseResponse() {

    }

    public BaseResponse(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
