package com.khmelenkoschlatter.hackzurich2015.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class AlertConfigRequest {

    @SerializedName("deviceId")
    private String deviceId;

    public AlertConfigRequest() {

    }

    public AlertConfigRequest(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }
}
