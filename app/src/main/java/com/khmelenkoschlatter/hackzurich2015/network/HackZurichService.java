package com.khmelenkoschlatter.hackzurich2015.network;

import com.khmelenkoschlatter.hackzurich2015.network.request.AddSensorRequest;
import com.khmelenkoschlatter.hackzurich2015.network.request.AlertConfigRequest;
import com.khmelenkoschlatter.hackzurich2015.network.request.LoginRequest;
import com.khmelenkoschlatter.hackzurich2015.network.request.RegistrationRequest;
import com.khmelenkoschlatter.hackzurich2015.network.request.UpdateAlertConfigRequest;
import com.khmelenkoschlatter.hackzurich2015.network.request.WeatherDataHistoryRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.LoginResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.RegistrationResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.SensorResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.WeatherDataResponse;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;

/**
 * Created by Donga on 03.10.2015.
 */
public interface HackZurichService {


    @POST("/register")
    RegistrationResponse register(@Body RegistrationRequest request);

    @POST("/login")
    LoginResponse login(@Body LoginRequest request);

    @POST("/addsensor")
    List<SensorResponse> addSensor(@Body AddSensorRequest request);

    @GET("/sensors")
    List<SensorResponse> getSensors();

    @POST("/history")
    List<WeatherDataResponse> getHistory(@Body WeatherDataHistoryRequest request);

    @POST("/alertconfig")
    AlertConfigResponse getAlertConfig(@Body AlertConfigRequest request);

    @POST("/updatealertconfig")
    AlertConfigResponse updateAlertConfig(@Body UpdateAlertConfigRequest request);

    @POST("/gcmtoken")
    String updatePushToken(@Body String token);

}
