package com.khmelenkoschlatter.hackzurich2015.network.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class AlertConfigResponse {

    @SerializedName("isTemperatureAlertEnabled")
    private boolean isTemperatureAlertEnabled;

    @SerializedName("minTemperature")
    private double minTemperature;

    @SerializedName("maxTemperature")
    private double maxTemperature;

    @SerializedName("isHumidityAlertEnabled")
    private boolean isHumidityAlertEnabled;

    @SerializedName("minHumidity")
    private double minHumidity;

    @SerializedName("maxHumidity")
    private double maxHumidity;


    public AlertConfigResponse() {
    }

    public boolean isTemperatureAlertEnabled() {
        return isTemperatureAlertEnabled;
    }

    public void setIsTemperatureAlertEnabled(boolean isTemperatureAlertEnabled) {
        this.isTemperatureAlertEnabled = isTemperatureAlertEnabled;
    }

    public double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public boolean isHumidityAlertEnabled() {
        return isHumidityAlertEnabled;
    }

    public void setIsHumidityAlertEnabled(boolean isHumidityAlertEnabled) {
        this.isHumidityAlertEnabled = isHumidityAlertEnabled;
    }

    public double getMinHumidity() {
        return minHumidity;
    }

    public void setMinHumidity(double minHumidity) {
        this.minHumidity = minHumidity;
    }

    public double getMaxHumidity() {
        return maxHumidity;
    }

    public void setMaxHumidity(double maxHumidity) {
        this.maxHumidity = maxHumidity;
    }
}
