package com.khmelenkoschlatter.hackzurich2015.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class RegistrationRequest {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;


    public RegistrationRequest() {
    }

    public RegistrationRequest(String email, String password) {

        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
