package com.khmelenkoschlatter.hackzurich2015.network.response;

import com.google.gson.annotations.SerializedName;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AccountResponse;

/**
 * Created by Donga on 03.10.2015.
 */
public class LoginResponse extends BaseResponse {

    @SerializedName("accountResponse")
    private AccountResponse accountResponse;

    public LoginResponse(int result) {
        super(result);
    }

    public LoginResponse(AccountResponse accountResponse) {
        super(SUCCESS);
        this.accountResponse = accountResponse;
    }

    public AccountResponse getAccountResponse() {
        return accountResponse;
    }
}
