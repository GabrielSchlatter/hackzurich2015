package com.khmelenkoschlatter.hackzurich2015.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class WeatherDataHistoryRequest {

    @SerializedName("sensorId")
    private String sensorId;

    public WeatherDataHistoryRequest() {

    }

    public WeatherDataHistoryRequest(String sensorId) {
        this.sensorId = sensorId;
    }

    public String getSensorId() {
        return sensorId;
    }
}
