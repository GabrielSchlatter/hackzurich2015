package com.khmelenkoschlatter.hackzurich2015.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class AddSensorRequest {

    @SerializedName("clientId")
    private String clientId;

    public AddSensorRequest() {

    }

    public AddSensorRequest(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }
}
