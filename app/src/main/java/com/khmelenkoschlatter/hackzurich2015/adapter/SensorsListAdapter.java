package com.khmelenkoschlatter.hackzurich2015.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.SensorResponse;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class SensorsListAdapter extends RecyclerView.Adapter<SensorsListAdapter.ViewHolder> {

    private List<SensorResponse> mSensors;
    private Context mContext;

    private OnItemListListener mListener;

    public SensorsListAdapter(Context context, List<SensorResponse> sensors, OnItemListListener listener) {
        mContext = context;
        mSensors = sensors;
        mListener = listener;
    }

    public void setSensors(List<SensorResponse> sensors) {
        mSensors = sensors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sensor, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SensorResponse sensor = mSensors.get(position);
        holder.mName.setText(sensor.getClientId());
    }

    @Override
    public int getItemCount() {
        return mSensors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mName;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            mName = (TextView) itemView.findViewById(R.id.item_sensor_name);
        }

        @Override
        public void onClick(View view) {
            if(mListener != null) {
                mListener.onItemSelected(getLayoutPosition());
            }
        }
    }
}
