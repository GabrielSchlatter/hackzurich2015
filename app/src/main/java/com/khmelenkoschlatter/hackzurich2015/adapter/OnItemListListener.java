package com.khmelenkoschlatter.hackzurich2015.adapter;

/**
 * @author Dmytro Khmelenko
 */
public interface OnItemListListener {

    void onItemSelected(int position);

}
