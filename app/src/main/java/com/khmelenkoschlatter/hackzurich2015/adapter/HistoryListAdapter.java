package com.khmelenkoschlatter.hackzurich2015.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khmelenkoschlatter.hackzurich2015.R;
import com.khmelenkoschlatter.hackzurich2015.network.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.WeatherDataResponse;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {

    private Context mContext;
    private List<WeatherDataResponse> mHistoryItems;

    public HistoryListAdapter(Context context, List<WeatherDataResponse> history) {
        mContext = context;
        mHistoryItems = history;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(mHistoryItems != null) {
            WeatherDataResponse data = mHistoryItems.get(position);

            holder.mTimestamp.setText(data.getTimestamp());

            String temperature = String.format("Temperature: %1$s", data.getTemperature());
            holder.mTemperature.setText(temperature);

            String humidity = String.format("Humidity: %1$s", data.getHumidity());
            holder.mHumidity.setText(humidity);
        }
    }

    @Override
    public int getItemCount() {
        return mHistoryItems != null ? mHistoryItems.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTimestamp;
        private TextView mTemperature;
        private TextView mHumidity;


        public ViewHolder(View itemView) {
            super(itemView);

            mTimestamp = (TextView) itemView.findViewById(R.id.item_history_timestamp);
            mTemperature = (TextView) itemView.findViewById(R.id.item_history_temperature);
            mHumidity = (TextView) itemView.findViewById(R.id.item_history_humidity);
        }
    }
}
