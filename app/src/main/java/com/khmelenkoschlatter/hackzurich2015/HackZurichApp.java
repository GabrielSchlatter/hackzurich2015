package com.khmelenkoschlatter.hackzurich2015;

import android.app.Application;
import android.content.Context;

/**
 * Created by Donga on 03.10.2015.
 */
public class HackZurichApp extends Application {

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return sContext;
    }
}
