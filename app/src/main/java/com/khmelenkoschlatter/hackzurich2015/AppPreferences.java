package com.khmelenkoschlatter.hackzurich2015;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Donga on 03.10.2015.
 */
public class AppPreferences {

    private static String PREFERENCES = "com.hackzurich.prefs";

    private static final String AUTH = "AuthHeader";

    public static void saveAuthorizationHeader(String authorizationHeader) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(HackZurichApp.getAppContext());
        pref.edit().putString(AUTH, authorizationHeader).commit();
    }

    public static String getAuthorizationHeader() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(HackZurichApp.getAppContext());
        return pref.getString(AUTH, null);
    }
}
