package com.khmelenkoschlatter.hackzurich2015.task;

/**
 * Created by Donga on 03.10.2015.
 */

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.network.BackendApi;

import de.greenrobot.event.EventBus;

/**
 * Defines an interface for all tasks
 */
public interface Task<T> {

    public EventBus mEventBus = EventBus.getDefault();

    /**
     * Starts executing the task
     *
     * @return Task result
     * @throws TaskException When error occurred during exection
     */
    abstract T execute() throws TaskException;

    /**
     * Is called in case of success task completion
     *
     * @param result Task result
     */
    abstract void onSuccess(T result);

    /**
     * Is called in case of the task failure
     *
     * @param error Task error
     */
    abstract void onFail(TaskError error);
}