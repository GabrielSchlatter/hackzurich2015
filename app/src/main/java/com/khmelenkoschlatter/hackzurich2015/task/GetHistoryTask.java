package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.HistorySuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.network.request.WeatherDataHistoryRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.WeatherDataResponse;
import com.khmelenkoschlatter.hackzurich2015.task.network.AuthenticatedNetworkTask;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class GetHistoryTask extends AuthenticatedNetworkTask<List<WeatherDataResponse>> {

    private final String mSensorName;

    public GetHistoryTask(String sensorName) {
        mSensorName = sensorName;
    }

    @Override
    public List<WeatherDataResponse> execute() throws TaskException {
        WeatherDataHistoryRequest request = new WeatherDataHistoryRequest(mSensorName);
        return mService.getHistory(request);
    }

    @Override
    public void onSuccess(List<WeatherDataResponse> result) {
        HistorySuccessEvent event = new HistorySuccessEvent(result);
        mEventBus.post(event);

    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
