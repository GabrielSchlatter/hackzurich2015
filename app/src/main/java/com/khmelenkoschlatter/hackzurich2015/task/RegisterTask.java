package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.RegistrationSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.BackendApi;
import com.khmelenkoschlatter.hackzurich2015.network.request.RegistrationRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.RegistrationResponse;

/**
 * @author Dmytro Khmelenko
 */
public class RegisterTask implements Task<RegistrationResponse> {

    private final String mEmail;
    private final String mPassword;
    private BackendApi mRestApi = BackendApi.getInstance();

    public RegisterTask(String email, String pass) {
        mEmail = email;
        mPassword = pass;
    }


    @Override
    public RegistrationResponse execute() throws TaskException {
        RegistrationRequest request = new RegistrationRequest(mEmail, mPassword);
        return mRestApi.getService().register(request);
    }

    @Override
    public void onSuccess(RegistrationResponse result) {
        RegistrationSuccessEvent event = new RegistrationSuccessEvent(result);
        mEventBus.post(event);
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
