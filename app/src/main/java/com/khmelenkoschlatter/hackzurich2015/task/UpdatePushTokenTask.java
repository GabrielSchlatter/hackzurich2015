package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.UpdatePushTokenSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.task.network.AuthenticatedNetworkTask;

/**
 * @author Dmytro Khmelenko
 */
public class UpdatePushTokenTask extends AuthenticatedNetworkTask<String> {

    private final String mToken;

    public UpdatePushTokenTask(String token) {
        mToken = token;
    }


    @Override
    public String execute() throws TaskException {
        return mService.updatePushToken(mToken);
    }

    @Override
    public void onSuccess(String result) {
        mEventBus.post(new UpdatePushTokenSuccessEvent());
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
