package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.FetchAleftConfigSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.network.request.AlertConfigRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.hackzurich2015.task.network.AuthenticatedNetworkTask;

/**
 * @author Dmytro Khmelenko
 */
public class FetchSensorConfigurationTask extends AuthenticatedNetworkTask<AlertConfigResponse> {

    private final String mSensorName;

    public FetchSensorConfigurationTask(String sensorName) {
        mSensorName = sensorName;
    }

    @Override
    public AlertConfigResponse execute() throws TaskException {
        AlertConfigRequest request = new AlertConfigRequest(mSensorName);
        return mService.getAlertConfig(request);
    }

    @Override
    public void onSuccess(AlertConfigResponse result) {
        FetchAleftConfigSuccessEvent event = new FetchAleftConfigSuccessEvent(result);
        mEventBus.post(event);
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
