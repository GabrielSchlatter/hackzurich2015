package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;

/**
 * Defines the results of the task execution
 */
public final class TaskResult<T> {
    private final T mResult;
    private final TaskError mError;

    TaskResult(T result, TaskError error) {
        mResult = result;
        mError = error;
    }

    T getResult() {
        return mResult;
    }

    boolean isSuccess() {
        return mError == null;
    }

    public TaskError getError() {
        return mError;
    }
}