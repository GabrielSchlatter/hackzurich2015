package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.UpdateAlertConfigSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.request.UpdateAlertConfigRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.hackzurich2015.task.network.AuthenticatedNetworkTask;

/**
 * @author Dmytro Khmelenko
 */
public class UpdateSensorConfigurationTask extends AuthenticatedNetworkTask<AlertConfigResponse> {

    private final String mSensorName;
    private final AlertConfigResponse mAlertConfig;

    public UpdateSensorConfigurationTask(String sensorName, AlertConfigResponse alertConfig) {
        mSensorName = sensorName;
        mAlertConfig = alertConfig;
    }

    @Override
    public AlertConfigResponse execute() throws TaskException {
        UpdateAlertConfigRequest request = new UpdateAlertConfigRequest(mSensorName, mAlertConfig);
        return mService.updateAlertConfig(request);
    }

    @Override
    public void onSuccess(AlertConfigResponse result) {
        UpdateAlertConfigSuccessEvent event = new UpdateAlertConfigSuccessEvent(result);
        mEventBus.post(event);
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
