package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.SensorsSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.SensorResponse;
import com.khmelenkoschlatter.hackzurich2015.task.network.AuthenticatedNetworkTask;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class GetSensorsTask extends AuthenticatedNetworkTask<List<SensorResponse>> {

    public GetSensorsTask() {

    }

    @Override
    public List<SensorResponse> execute() throws TaskException {
        return mService.getSensors();
    }

    @Override
    public void onSuccess(List<SensorResponse> result) {
        SensorsSuccessEvent event = new SensorsSuccessEvent(result);
        mEventBus.post(event);
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
