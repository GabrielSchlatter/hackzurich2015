package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;
import com.khmelenkoschlatter.hackzurich2015.event.LoadingFailedEvent;
import com.khmelenkoschlatter.hackzurich2015.event.LoginSuccessEvent;
import com.khmelenkoschlatter.hackzurich2015.network.BackendApi;
import com.khmelenkoschlatter.hackzurich2015.network.request.LoginRequest;
import com.khmelenkoschlatter.hackzurich2015.network.response.LoginResponse;

/**
 * @author Dmytro Khmelenko
 */
public class LoginTask implements Task<LoginResponse> {

    private final String mEmail;
    private final String mPassword;
    private BackendApi mRestApi = BackendApi.getInstance();

    public LoginTask(String email, String password) {
        mEmail = email;
        mPassword = password;
    }

    @Override
    public LoginResponse execute() throws TaskException {
        LoginRequest request = new LoginRequest(mEmail, mPassword);
        return mRestApi.getService().login(request);
    }

    @Override
    public void onSuccess(LoginResponse result) {
        LoginSuccessEvent event = new LoginSuccessEvent(result);
        mEventBus.post(event);
    }

    @Override
    public void onFail(TaskError error) {
        LoadingFailedEvent event = new LoadingFailedEvent(error);
        mEventBus.post(event);
    }
}
