package com.khmelenkoschlatter.hackzurich2015.task;

import android.os.AsyncTask;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;
import com.khmelenkoschlatter.hackzurich2015.error.TaskException;

/**
 * Defines async task for executing custom user tasks {@link Task}
 */
public final class LoadingAsyncTask<T> extends AsyncTask<Void, Void, TaskResult<T>> {

    private final Task<T> mTask;

    public LoadingAsyncTask(Task<T> task) {
        mTask = task;
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected TaskResult<T> doInBackground(Void... dataTasks) {
        TaskResult result;
        try {
            T executionResult = mTask.execute();
            result = new TaskResult<T>(executionResult, null);
        } catch (TaskException e) {
            TaskError error = new TaskError(e.getCode(), e.getMessage());
            result = new TaskResult<T>(null, error);
        }
        return result;
    }

    @Override
    protected void onPostExecute(TaskResult<T> taskResult) {
        if (taskResult.isSuccess()) {
            mTask.onSuccess(taskResult.getResult());
        } else {
            mTask.onFail(taskResult.getError());
        }
    }
}