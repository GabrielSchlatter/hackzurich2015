package com.khmelenkoschlatter.hackzurich2015.task;

import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;

/**
 * Created by Donga on 03.10.2015.
 */
public final class TaskLauncher {

    private static void launchTask(Task task) {
        new LoadingAsyncTask<>(task);
    }

    public static void login(String email, String password) {
        launchTask(new LoginTask(email, password));
    }

    public static void register(String email, String password) {
        launchTask(new RegisterTask(email, password));
    }

    public static void getSensors() {
        launchTask(new GetSensorsTask());
    }


    public static void addSensor(String sensorName) {
        launchTask(new AddSensorTask(sensorName));
    }

    public static void getHistory(String sensorName) {
        launchTask(new GetHistoryTask(sensorName));
    }

    public static void fetchAlertConfig(String sensorName) {
        launchTask(new FetchSensorConfigurationTask(sensorName));
    }

    public static void updateAlertConfig(String sensorName, AlertConfigResponse data) {
        launchTask(new UpdateSensorConfigurationTask(sensorName, data));
    }

    public static void updatePushToken(String token) {
        launchTask(new UpdatePushTokenTask(token));
    }
}
