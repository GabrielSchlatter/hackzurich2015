package com.khmelenkoschlatter.hackzurich2015.task.network;

import com.khmelenkoschlatter.hackzurich2015.AppPreferences;
import com.khmelenkoschlatter.hackzurich2015.network.BackendApi;
import com.khmelenkoschlatter.hackzurich2015.network.HackZurichService;
import com.khmelenkoschlatter.hackzurich2015.task.Task;

/**
 * Created by Gabriel on 22.09.2015.
 */
public abstract class AuthenticatedNetworkTask<T> implements Task<T> {

    protected HackZurichService mService;

    public AuthenticatedNetworkTask() {
        String authHeader = AppPreferences.getAuthorizationHeader();
        mService = BackendApi.getInstance(authHeader).getService();
    }
}
