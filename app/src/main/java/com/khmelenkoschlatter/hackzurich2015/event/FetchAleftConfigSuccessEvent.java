package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;

/**
 * @author Dmytro Khmelenko
 */
public class FetchAleftConfigSuccessEvent {

    private final AlertConfigResponse mAlertConfig;

    public FetchAleftConfigSuccessEvent(AlertConfigResponse alertConfig) {
        mAlertConfig = alertConfig;
    }

    public AlertConfigResponse getAlertConfig() {
        return mAlertConfig;
    }
}
