package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.RegistrationResponse;

/**
 * @author Dmytro Khmelenko
 */
public class RegistrationSuccessEvent {

    private final RegistrationResponse mResponse;


    public RegistrationSuccessEvent(RegistrationResponse response) {
        mResponse = response;
    }

    public RegistrationResponse getResponse() {
        return mResponse;
    }
}
