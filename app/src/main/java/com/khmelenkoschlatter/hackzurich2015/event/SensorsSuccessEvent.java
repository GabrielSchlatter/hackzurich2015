package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.model.SensorResponse;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class SensorsSuccessEvent {

    private final List<SensorResponse> mSensors;


    public SensorsSuccessEvent(List<SensorResponse> sensors) {
        mSensors = sensors;
    }

    public List<SensorResponse> getSensors() {
        return mSensors;
    }
}
