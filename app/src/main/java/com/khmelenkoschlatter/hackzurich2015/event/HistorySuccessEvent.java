package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.hackzurich2015.network.response.model.WeatherDataResponse;

import java.util.List;

/**
 * @author Dmytro Khmelenko
 */
public class HistorySuccessEvent {

    private final List<WeatherDataResponse> mHistoryData;

    public HistorySuccessEvent(List<WeatherDataResponse> historyData) {
        mHistoryData = historyData;
    }

    public List<WeatherDataResponse> getHistoryData() {
        return mHistoryData;
    }
}
