package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.error.TaskError;

/**
 * @author Dmytro Khmelenko
 */
public class LoadingFailedEvent {

    private final TaskError mTaskError;

    public LoadingFailedEvent(TaskError taskError) {
        mTaskError = taskError;
    }

    public TaskError getTaskError() {
        return mTaskError;
    }
}
