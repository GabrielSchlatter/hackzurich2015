package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.LoginResponse;

/**
 * @author Dmytro Khmelenko
 */
public class LoginSuccessEvent {

    private final LoginResponse mLoginResponse;

    public LoginSuccessEvent(LoginResponse loginResponse) {
        mLoginResponse = loginResponse;
    }

    public LoginResponse getLoginResponse() {
        return mLoginResponse;
    }
}
