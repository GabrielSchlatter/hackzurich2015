package com.khmelenkoschlatter.hackzurich2015.event;

import com.khmelenkoschlatter.hackzurich2015.network.response.model.AlertConfigResponse;

/**
 * @author Dmytro Khmelenko
 */
public class UpdateAlertConfigSuccessEvent {

    private final AlertConfigResponse mAlertConfig;

    public UpdateAlertConfigSuccessEvent(AlertConfigResponse alertConfig) {
        mAlertConfig = alertConfig;
    }

    public AlertConfigResponse getAlertConfig() {
        return mAlertConfig;
    }
}
